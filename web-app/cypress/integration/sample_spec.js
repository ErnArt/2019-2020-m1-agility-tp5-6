// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scenario 1 - Does configuration button works', () => {
    it('it goes to configuration', () => {

        // We click on the configuration button and verify it the url includes "configuration"
        cy.get('a[href="#configuration"]').click()
        cy.url().should('include', 'configuration')
    })
})

describe('Scenario 2 - Is there 28 segments', () => {
    it('There are 28 segments', () => {
        cy.get('a[href="#configuration"]').click()

        // With should we verify if the length of the form table segment is 28
        cy.get('form[data-kind="segment"]').should('have.length', 28)
    })
})

describe('Scenario 3 - Is there no car', () => {
    it('There is no car', () => {
        cy.get('a[href="#configuration"]').click()

        // We search any table that contains 'No Vehicle available'
        cy.get('table').contains('No vehicle available')
    })
})

describe('Scenario 4 - Does the segment 5 has 30', () => {
    it('Going to configuration', () => {
        cy.get('a[href="#configuration"]').click()
    })
    it('Updating our segment', () => {
        // We erase what was in the input form and change it to 30.
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30')
        cy.get('form[id="segment-5"]').click()

        // We click on the update window's button to save them.
        cy.get('.modal-footer > .btn').click()
    })
    it('The segment 5 has 30', () => {
        // We save as the @elements the request.
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')

        cy.get('@elements').should((response) => {

            // We verify that our line 4 has 30 for its speed.
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })
    })
})

describe('Scenario 5 - Modify the roundabout', () => {
    it('Updating the roundabout', () => {
        cy.get('a[href="#configuration"]').click()
        // Here we update the roundabout with typing new values and updating it.
        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').clear().type('4')
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').clear().type('15')
        cy.get(':nth-child(3) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('The roundabout has 4 and 15 seconds', () => {
        // The reload is here to be sure our data are saved.
        cy.reload()
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            // As for the previous segments we verify that our roundabout located in
            // the third element of crossroads has the right properties.
            expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
            expect(response.body['crossroads'][2]).to.have.property('duration', 15)
        })
    })
})

describe('Scenario 6 - Modify the light traffic', () => {
    it('Updating the light traffic', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type('4')
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type('40')
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type('8')
        cy.get(':nth-child(1) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('The 29 Trafficlight has 4 - 40 - 8', () => {
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('Scenario 7 - Add vehicles', () => {
    it('Adding first vehicle', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Adding second vehicle', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Adding third vehicle', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('The vehicles were added', () => {
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            // Here we verify that the vehicles are defined, to be sure they are created and saved.
            expect(response.body['50.0'])
            expect(response.body['150.0'])
            expect(response.body['200.0'])
        })
    })
})

describe('Story 8 - Are vehicles in the simulation', () => {
    it('Verifying the end of the progress bar', () => {
        cy.get('a[href="#simulation"]').click()
        // We first verify that the progress bar is at 0%.
        cy.get('.progress-bar').should('have.attr', 'aria-valuenow', '0')

        cy.get('.form-control').clear().type('120')
        cy.get('.btn').click()
        // Here we wait the end of the simulation (with extra time to be sure) to verify
        // the aria-valuenow attribute of the progress-bar.
        cy.get('.progress-bar', {timeout: 500 * 1000}).should('have.attr', 'aria-valuenow', '100')
    })

    it('Each vehicles are at stop and the first vehicle has ended the simulation', () => {
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(response.body['50.0'][0]).to.have.property('speed', 10)
        })
    })
})

describe('Story 9 - Have the simulation been resetted and vehicles not moving', () => {
    it('Resetting the simulation', () => {
        cy.request('GET', 'http://127.0.0.1:3000/init')
        cy.reload()
        cy.get('a[href="#configuration"]').click()
        cy.get('table').contains('No vehicle available')
    })

    it('Creating the vehicles', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Verifying the progress bar', () => {
        cy.get('a[href="#simulation"]').click()
        cy.get('.progress-bar').should('have.attr', 'aria-valuenow', '0')
        cy.get('.form-control').clear().type('500')
        cy.get('.btn').click()
        cy.get('.progress-bar', {timeout: 600 * 1000}).should('have.attr', 'aria-valuenow', '100')
    })
    
    it('Vehicles are not moving', () => {
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            // Here we use simple js code to verify that the json is empty.
            if (!Object.keys(response).length) { }
        })
    })
})

describe('Scenario 10 - Are vehicles in the Segment 17 - 17 - 11', () => {
    it('Resetting the simulation', () => {
        // We need to reload after requesting the initialise of the program.
        cy.request('GET', 'http://127.0.0.1:3000/init')
        cy.reload()
        cy.get('a[href="#configuration"]').click()
        cy.get('table').contains('No vehicle available')
    })

    it('Creating the vehicles', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Verifying the progress bar', () => {
        cy.get('a[href="#simulation"]').click()
        cy.get('.progress-bar').should('have.attr', 'aria-valuenow', '0')
        cy.get('.form-control').clear().type('200')
        cy.get('.btn').click()
        cy.get('.progress-bar', {timeout: 300 * 1000}).should('have.attr', 'aria-valuenow', '100')
    })

    it('The vehicles are in the right segment', () => {
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            // We verify that each vehicles has in its step attribute the right segment.
            expect(response.body['50.0'][0]['step']).to.have.property('id', 17)
            expect(response.body['80.0'][0]['step']).to.have.property('id', 29)
            expect(response.body['80.0'][1]['step']).to.have.property('id', 29)
        })
    })
})